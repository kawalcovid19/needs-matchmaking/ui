export const state = () => ({});

export const getters = {
  markers(state, getters, rootState) {
    // return rootState.beneficiaryList.dummy_list_data.map(data => {
    //   return {
    //     _id: data._id,
    //     latLng: {
    //       lat: data.location.latitude,
    //       lng: data.location.longitude,
    //     },
    //   };
    // });
    return rootState.beneficiaryList.list_data.map(data => {
      return {
        _id: data._id,
        latLng: {
          lat: data.location.latitude,
          lng: data.location.longitude,
        },
      };
    });
  },
};
export const mutations = {
  SET_MARKERS(state, payload) {
    state.markers = payload;
  },
};

export const actions = {};
