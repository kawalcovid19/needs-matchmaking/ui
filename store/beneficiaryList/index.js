export const state = () => ({
  loading: false,
  list_data: [],
  dummy_list_data: [
    {
      _id: '4a618aaf-56bf-4b5b-bced-a8c2c6467151',
      name: 'masker untuk puskesmas',
      category: 'masker',
      provider_name: 'dr wardoyo',
      amount: 100,
      receiver_name: 'dr wardoyo',
      phone: '081234567890',
      is_whatsapp_available: false,
      note: 'puskesmas hanya buka di jam kerja',
      is_active: true,
      created_at: '2020-04-01T13:49:05.805582Z',
      updated_at: '``',
      item_details: [
        {
          name: 'masker bedah',
          quantity: 10,
        },
      ],
      location: {
        name: 'Slipi Petamburan',
        country: 'Indonesia',
        city: 'Jakarta Pusat',
        address:
          'Jl. Pejompongan Dalam 1-29, RT.4/RW.5, Bend. Hilir, Kecamatan Tanah Abang, Daerah Khusus Ibukota Jakarta 10210',
        longitude: 106.800685,
        latitude: 6.204318,
      },
      distance: {
        length: 1.1809069286636493,
        unit: 'km',
      },
    },
    {
      _id: '4a618aaf-56bf-4b5b-bced-a8c2c6467152',
      name: 'masker untuk puskesmas 2',
      category: 'masker',
      provider_name: 'dr wardoyo',
      amount: 100,
      receiver_name: 'dr wardoyo',
      phone: '081234567890',
      is_whatsapp_available: false,
      note: 'puskesmas hanya buka di jam kerja',
      is_active: true,
      created_at: '2020-04-01T13:49:05.805582Z',
      updated_at: '``',
      item_details: [
        {
          name: 'masker bedah',
          quantity: 10,
        },
      ],
      location: {
        name: 'Slipi Petamburan',
        country: 'Indonesia',
        city: 'Jakarta Pusat',
        address:
          'Jl. Pejompongan Dalam 1-29, RT.4/RW.5, Bend. Hilir, Kecamatan Tanah Abang, Daerah Khusus Ibukota Jakarta 10210',
        longitude: 105.700685,
        latitude: 6.104318,
      },
      distance: {
        length: 1.1809069286636493,
        unit: 'km',
      },
    },
    {
      _id: '4a618aaf-56bf-4b5b-bced-a8c2c6467153',
      name: 'masker untuk puskesmas 3',
      category: 'masker',
      provider_name: 'dr wardoyo',
      amount: 100,
      receiver_name: 'dr wardoyo',
      phone: '081234567890',
      is_whatsapp_available: false,
      note: 'puskesmas hanya buka di jam kerja',
      is_active: true,
      created_at: '2020-04-01T13:49:05.805582Z',
      updated_at: '``',
      item_details: [
        {
          name: 'masker bedah',
          quantity: 10,
        },
      ],
      location: {
        name: 'Slipi Petamburan',
        country: 'Indonesia',
        city: 'Jakarta Pusat',
        address:
          'Jl. Pejompongan Dalam 1-29, RT.4/RW.5, Bend. Hilir, Kecamatan Tanah Abang, Daerah Khusus Ibukota Jakarta 10210',
        longitude: 104.600685,
        latitude: 6.004318,
      },
      distance: {
        length: 1.1809069286636493,
        unit: 'km',
      },
    },
  ],
});

export const getters = {
  isCompleteFormModalShow(state) {
    return state.completeFormModal.show;
  },
  completeFormModalAction(state) {
    return state.completeFormModal.action;
  },
};

export const mutations = {
  SET_LOADING(state, payload) {
    state.loading = payload;
  },
  SET_LIST_DATA(state, payload) {
    state.list_data = payload;
  },
};

export const actions = {
  restoreInitialState({ commit }) {
    commit('SET_LOADING', false);
    commit('SET_LIST_DATA', []);
  },

  async fetchListData({ commit, rootState, dispatch }) {
    try {
      commit('SET_LOADING', true);
      const { data } = await this.$axios.get('/beneficiary', {
        params: {
          find_nearby: rootState.clientInfo.location !== null,
          latitude: rootState.clientInfo.location?.lat,
          longitude: rootState.clientInfo.location?.lng,
        },
      });
      let listData = data.data;
      listData = await dispatch('formatDistance', data.data);
      commit('SET_LIST_DATA', listData);
    } finally {
      commit('SET_LOADING', false);
    }
  },
  formatDistance(context, listData) {
    listData.forEach(element => {
      element.distance = `${Math.round(element.distance.length)} ${element.distance.unit}`;
    });
    return listData;
  },
};
