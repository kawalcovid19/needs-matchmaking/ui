export const state = () => ({
  loading: false,
  list_data: [],
});

export const getters = {
  isCompleteFormModalShow(state) {
    return state.completeFormModal.show;
  },
  completeFormModalAction(state) {
    return state.completeFormModal.action;
  },
};

export const mutations = {
  SET_LOADING(state, payload) {
    state.loading = payload;
  },
  SET_LIST_DATA(state, payload) {
    state.list_data = payload;
  },
};

export const actions = {
  restoreInitialState({ commit }) {
    commit('SET_LOADING', false);
    commit('SET_LIST_DATA', []);
  },

  async fetchListData({ commit, rootState, dispatch }) {
    try {
      commit('SET_LOADING', true);
      const { data } = await this.$axios.get('/contributor', {
        params: {
          find_nearby: rootState.clientInfo.location !== null,
          latitude: rootState.clientInfo.location?.lat,
          longitude: rootState.clientInfo.location?.lng,
        },
      });
      let listData = data.data;
      listData = await dispatch('formatDistance', data.data);
      commit('SET_LIST_DATA', listData);
    } finally {
      commit('SET_LOADING', false);
    }
  },
  formatDistance(context, listData) {
    listData.forEach(element => {
      element.distance = `${Math.round(element.distance.length)} ${element.distance.unit}`;
    });
    return listData;
  },
};
