export const state = () => ({
  type: 'C', // C: contributor || B: beneficiary
  loading: false,
  agree: false,
  address: null,
  lat: null,
  lng: null,
  title: null,
  note: null,
  provider_name: null,
  phone_number: '',
  can_show_phone_number: false,
  wa_available: false,
  item_details: [
    {
      name: '',
      qty: 1,
    },
  ],
  active_tab: 1,
  active_component: 'Section1',
  defaultCenter: {
    lat: -6.1753924,
    lng: 106.8249641,
  },
  loadingReverseGeocode: false,
});

export const getters = {
  validItemDetails(state) {
    return state.item_details.filter(function(item) {
      return item.name !== null && item.name !== '';
    });
  },
  formData(state, getters) {
    let formData = {
      name: state.title,
      category: '',
      amount: 1,
      provider_name: state.provider_name,
      phone: state.phone_number,
      is_whatsapp_available: state.wa_available,
      note: state.note,
      location: {
        // name: 'Slipi',
        address: state.address,
        latitude: state.lat,
        longitude: state.lng,
      },
    };
    if (state.type === 'C') {
      formData.is_phone_number_hidden = state.can_show_phone_number;
    }
    formData.item_details = getters.validItemDetails.map(elem => {
      return {
        name: elem.name,
        quantity: elem.qty,
      };
    });
    return formData;
  },
};

export const mutations = {
  SET_TYPE(state, payload) {
    state.type = payload;
  },
  SET_LOADING_REVERSE_GEOCODE(state, payload) {
    state.loadingReverseGeocode = payload;
  },
  SET_ACTIVE_TAB(state, payload) {
    state.active_tab = payload;
  },
  SET_ACTIVE_COMPONENT(state, payload) {
    state.active_component = payload;
  },
  SET_LOADING(state, payload) {
    state.loading = payload;
  },
  SET_AGREE(state, payload) {
    state.agree = payload;
  },

  SET_ADDRESS(state, payload) {
    state.address = payload;
  },
  SET_LAT(state, payload) {
    state.lat = payload;
  },

  SET_LNG(state, payload) {
    state.lng = payload;
  },

  SET_TITLE(state, payload) {
    state.title = payload;
  },

  SET_NOTE(state, payload) {
    state.note = payload;
  },

  SET_PROVIDER_NAME(state, payload) {
    state.provider_name = payload;
  },

  SET_PHONE_NUMBER(state, payload) {
    state.phone_number = payload;
  },
  SET_CAN_SHOW_PHONE_NUMBER(state, payload) {
    state.can_show_phone_number = payload;
  },
  SET_WA_AVAILABLE(state, payload) {
    state.wa_available = payload;
  },
  SET_ITEM_DETAILS(state, payload) {
    state.item_details = payload;
  },
  ADD_ITEM_DETAILS(state, { name, qty }) {
    state.item_details.push({ name, qty });
  },
  REMOVE_ITEM_DETAILS(state, index) {
    state.item_details.splice(index, 1);
  },
};

export const actions = {
  restoreInitialState({ commit }) {
    commit('SET_LOADING', false);
    commit('SET_LOADING_REVERSE_GEOCODE', false);
    commit('SET_TYPE', 'C');
    commit('SET_ACTIVE_TAB', 1);
    commit('SET_ACTIVE_COMPONENT', 'Section1');
    commit('SET_ITEM_DETAILS', [
      {
        name: '',
        qty: 1,
      },
    ]);
    commit('SET_WA_AVAILABLE', null);
    commit('SET_CAN_SHOW_PHONE_NUMBER', false);
    commit('SET_PHONE_NUMBER', null);
    commit('SET_PROVIDER_NAME', null);
    commit('SET_NOTE', null);
    commit('SET_AGREE', false);
    commit('SET_TITLE', null);
    commit('SET_LNG', null);
    commit('SET_LAT', null);
    commit('SET_ADDRESS', null);
  },
  switchActiveTab({ commit }, value) {
    commit('SET_ACTIVE_TAB', value);
    commit('SET_ACTIVE_COMPONENT', `Section${value}`);
  },
  async postData({ state, commit, getters }) {
    try {
      commit('SET_LOADING', true);
      const url = state.type === 'C' ? '/contributor' : '/beneficiary';
      await this.$axios.post(url, getters.formData);
    } finally {
      commit('SET_LOADING', false);
    }
  },
  async fetchCurrentPosition({ state, commit }) {
    if (state.location === null) {
      try {
        const pos = await new Promise(function(resolve, reject) {
          navigator.geolocation.getCurrentPosition(resolve, reject);
        });
        commit('SET_LOCATION', {
          lat: pos.coords.latitude,
          lng: pos.coords.longitude,
        });
      } catch (e) {
        console.error(e);
      }
    }
  },
};
