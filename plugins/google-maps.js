import Vue from 'vue';
import * as GmapVue from 'gmap-vue';
import GmapCluster from 'gmap-vue/dist/components/cluster'; // replace src with dist if you have Babel issues

Vue.use(GmapVue, {
  load: { key: 'AIzaSyAqNDrJhXjrnDWLE3E4wd3kk3o0kXAjpfE', libraries: 'places' },
});
Vue.component('GmapCluster', GmapCluster);
