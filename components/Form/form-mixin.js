export default {
  computed: {
    formType: state => state.form.type,
  },
  methods: {
    showSuccessAdd() {
      let title, html;
      if (this.formType === 'C') {
        title = 'Bantuan Berhasil Didaftarkan!';
        html = 'Pencari bantuan tidak akan melihat titik bantuan ini lagi.';
      } else {
        title = 'Permintaan Berhasil Didaftarkan!';
        html = 'Anda akan dihubungi langsung oleh calon pemberi bantuan.';
      }

      return this.$swal({
        title: title,
        html: html,
        icon: 'success',
        showConfirmButton: false,
        showCloseButton: true,
      });
    },
    showNotAgreeWarning() {
      return this.$swal({
        title: 'Anda belum menyetujui syarat dan ketentuan',
        html: 'Mohon membaca dan menyetujui syarat dan ketentuan untuk melanjutkan ke halaman berikutnya.',
        icon: 'error',
        showConfirmButton: false,
        showCloseButton: true,
      });
    },
    showItemDetailsEmpty() {
      return this.$swal({
        title: 'Anda belum menambahkan rincian bantuan',
        html: 'Mohon menambahkan minimal 1 barang bantuan di kolom <b>Rincian Bantuan</b>',
        icon: 'error',
        showConfirmButton: false,
        showCloseButton: true,
      });
    },
    showFormNotComplete() {
      return this.$swal({
        title: 'Anda belum selesai mengisi formulir',
        text: 'Mohon cek kembali formulri Anda dan isi semua kolom untuk melanjutkan kehalaman berikutnya.',
        icon: 'error',
        showConfirmButton: false,
        showCloseButton: true,
      });
    },
    showCancelWarning() {
      return this.$swal({
        title: 'Batal Pengisian?',
        text: 'Jika membatalkan, Anda perlu mengisi formulir ini dari awal di kemudian hari.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, batalkan',
        cancelButtonText: 'Tidak, lanjutkan isi formulir',
        showCloseButton: true,
      }).then(result => {
        if (result.value === true) {
          this.$router.push('/');
        }
      });
    },
  },
};
